package com.epam.bubnii.dto;

import com.epam.bubnii.entity.*;

import java.util.List;

public interface NewsDao extends CrudDao<News> {

    List<News> findAllByCategory(Category category);

}
