package com.epam.bubnii.dto;

import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {

    List<T> findAll();

    Optional<T> findById(Integer id);

    Integer create(T model);

    void update(T model);

}
