package com.epam.bubnii.dto;

import com.epam.bubnii.entity.Category;
import com.epam.bubnii.entity.News;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class NewsDaoImpl implements NewsDao {

    private final String SQL_UPDATE = "UPDATE news SET title = ?, description = ?, bodyNews = ?, linkToNews = ?, category = ? WHERE ID = ?";
    private final String SQL_SELECT_ALL = "SELECT * FROM news";
    private final String SQL_SELECT_FIND_BY_CATEGORY = "SELECT * FROM news WHERE category = ?";
    private final String SQL_SELECT_FIND_BY_ID = "SELECT * FROM news WHERE id = ?";
    private final String SQL_INSERT = "INSERT INTO news(title, description, bodyNews, linkToNews,category) values (?,?,?,?,?)";

    private static Logger LOGGER = LogManager.getLogger(NewsDaoImpl.class);
    private Connection connection;

    public NewsDaoImpl() {
    }

    public NewsDaoImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<News> findAllByCategory(Category category) {
        LOGGER.info("Run the method findAllByCategory with the parameter: category = " + category);
        List<News> newsList = new ArrayList<>();
        LOGGER.info("Creating PreparedStatement...");
        try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_FIND_BY_CATEGORY)) {
            statement.setString(1, category.toString());
            LOGGER.info("Creating ResultSet...");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String title = resultSet.getString("title");
                String description = resultSet.getString("description");
                String bodyNews = resultSet.getString("bodyNews");
                String linkToNews = resultSet.getString("linkToNews");

                News news = new News(id, title, description, bodyNews, linkToNews, category);
                newsList.add(news);
            }
        } catch (SQLException e) {
            LOGGER.info("catch Exception " + e.fillInStackTrace());
            e.printStackTrace();
        }
        return newsList;
    }

    @Override
    public List<News> findAll() {
        LOGGER.info("Run the method findAll");
        List<News> newsList = new ArrayList<>();
        LOGGER.info("Creating PreparedStatement...");
        try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            LOGGER.info("Creating ResultSet...");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String title = resultSet.getString("title");
                String description = resultSet.getString("description");
                String bodyNews = resultSet.getString("bodyNews");
                String linkToNews = resultSet.getString("linkToNews");
                Category category = Category.valueOf(String.valueOf(resultSet.getObject("category")));

                News news = new News(id, title, description, bodyNews, linkToNews, category);
                newsList.add(news);
            }
        } catch (SQLException e) {
            LOGGER.info("catch Exception " + e.fillInStackTrace());
            e.printStackTrace();
        }
        return newsList;
    }

    @Override
    public Optional<News> findById(Integer id) {
        LOGGER.info("Run the method findById with the parameter: id = " + id);
        LOGGER.info("Creating PreparedStatement...");
        try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_FIND_BY_ID)) {
            statement.setInt(1, id);
            LOGGER.info("Creating ResultSet...");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String title = resultSet.getString("title");
                String description = resultSet.getString("description");
                String bodyNews = resultSet.getString("bodyNews");
                String linkToNews = resultSet.getString("linkToNews");
                Category category = Category.valueOf(String.valueOf(resultSet.getObject("category")));
                return Optional.of(new News(id, title, description, bodyNews, linkToNews, category));
            }
        } catch (SQLException e) {
            LOGGER.info("catch Exception " + e.fillInStackTrace());
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Integer create(News model) {
        LOGGER.info("Run the method create with the parameter: title = " + model.getTitle()
                + ", category = " + model.getCategory());
        int candidateId = 0;
        LOGGER.info("Creating PreparedStatement...");
        try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            LOGGER.info("exclusion of the transaction auto commit");
            connection.setAutoCommit(false);
            ResultSet resultSet;
            statement.setString(1, model.getTitle());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getBodyNews());
            statement.setString(4, model.getLinkToNews());
            statement.setObject(5, model.getCategory().toString());
            int rowAffected = statement.executeUpdate();
            if (rowAffected == 1) {
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    candidateId = resultSet.getInt(1);
                    LOGGER.info("candidate id = " + candidateId);
                }
            }
            LOGGER.info("transaction commit");
            connection.commit();
        } catch (SQLException e) {
            LOGGER.info("catch Exception " + e.fillInStackTrace());
            e.printStackTrace();
        }
        return candidateId;
    }

    @Override
    public void update(News model) {
        LOGGER.info("Run the method update with the parameter: id = " + model.getId());
        LOGGER.info("Creating PreparedStatement...");
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            LOGGER.info("exclusion of the transaction auto commit");
            connection.setAutoCommit(false);
            statement.setString(1, model.getTitle());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getBodyNews());
            statement.setString(4, model.getLinkToNews());
            statement.setObject(5, model.getCategory().toString());
            statement.setInt(6, model.getId());
            statement.executeUpdate();
            LOGGER.info("transaction commit");
            connection.commit();
        } catch (SQLException e) {
            LOGGER.info("catch Exception " + e.fillInStackTrace());
            e.printStackTrace();
        }
    }
}
