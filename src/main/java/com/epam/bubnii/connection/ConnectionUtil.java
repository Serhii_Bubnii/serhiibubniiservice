package com.epam.bubnii.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/newspaper?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    private static Logger LOGGER = LogManager.getLogger(ConnectionUtil.class);
    private static Connection connection;

    private ConnectionUtil(){
    }

    public static Connection getConnectionUtil(){
        try {
            LOGGER.info("Registering JDBC driver...");
            Class.forName(JDBC_DRIVER);
            if (connection == null){
                LOGGER.info("Connection to database...");
                connection = DriverManager.getConnection(DB_URL,USER,PASSWORD);
            }
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.info("catch Exception " + e.fillInStackTrace());
            throw new ExceptionInInitializerError(e);
        }
        return connection;
    }
}
