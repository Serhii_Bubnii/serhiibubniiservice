package com.epam.bubnii.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class News {
    private Integer id;
    private String title;
    private String description;
    private String bodyNews;
    private String  linkToNews;
    private Category category;

    public News(String title, String description, String bodyNews, String linkToNews, Category category) {
        this.title = title;
        this.description = description;
        this.bodyNews = bodyNews;
        this.linkToNews = linkToNews;
        this.category = category;
    }
}
