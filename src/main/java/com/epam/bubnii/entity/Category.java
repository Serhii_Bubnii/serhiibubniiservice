package com.epam.bubnii.entity;

public enum Category {
    POLITICAL,
    SPORT,
    ECONOMIC
}
