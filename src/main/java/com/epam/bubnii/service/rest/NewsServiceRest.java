package com.epam.bubnii.service.rest;

import com.epam.bubnii.entity.Category;
import com.epam.bubnii.entity.News;

import javax.ws.rs.core.Response;
import java.util.List;

public interface NewsServiceRest {

    List<News> findAll();

    News findById(Integer id);

    Response create(News model);

    Response update(News model);

    List<News> findAllByCategory(Category category);
}
