package com.epam.bubnii.service.rest;

import com.epam.bubnii.dto.NewsDao;
import com.epam.bubnii.entity.Category;
import com.epam.bubnii.entity.News;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/news")
public class NewsServiceRestImpl implements NewsServiceRest {

    private static Logger LOGGER = LogManager.getLogger(NewsServiceRestImpl.class);

    private NewsDao newsDao;

    public NewsServiceRestImpl() {
    }

    public NewsServiceRestImpl(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    @GET
    @Path("/by/{category}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public List<News> findAllByCategory(@PathParam("category") Category category) {
        LOGGER.info("REST GET method findAllByCategory() with PathParam = " + category.toString());
        return newsDao.findAllByCategory(category);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public List<News> findAll() {
        LOGGER.info("REST GET method findAll()");
        return newsDao.findAll();
    }

    @GET
    @Path("/by-query")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public News findById(@QueryParam("id") Integer id) {
        LOGGER.info("REST GET method findById() with QueryParam = " + id);
        return newsDao.findById(id).get();
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response create(News model) {
        LOGGER.info("REST POST method to create() for such model = " + model);
        newsDao.create(model);
        return Response.status(201).entity(model).build();
    }

    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response update(News model) {
        LOGGER.info("REST PUT method to update() for such model = " + model);
        newsDao.update(model);
        return Response.ok().entity(model).build();
    }
}
