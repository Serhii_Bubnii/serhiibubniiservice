package com.epam.bubnii.service.soap;

import com.epam.bubnii.entity.Category;
import com.epam.bubnii.entity.News;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface NewsServiceSoap {
    @WebMethod
    List<News> findAll();

    @WebMethod
    News findById(Integer id);

    @WebMethod
    Integer create(News model);

    @WebMethod
    void update(News model);
    @WebMethod
    List<News> findAllByCategory(Category category);

}
