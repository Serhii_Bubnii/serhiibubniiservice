package com.epam.bubnii.service.soap;

import com.epam.bubnii.dto.NewsDao;
import com.epam.bubnii.entity.Category;
import com.epam.bubnii.entity.News;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "com.epam.bubnii.service.soap.NewsServiceSoap")
@NoArgsConstructor
public class NewsServiceSoapImpl implements NewsServiceSoap {

    private static Logger LOGGER = LogManager.getLogger(NewsServiceSoapImpl.class);

    private NewsDao newsDao;

    public NewsServiceSoapImpl(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    @Override
    @WebMethod
    public List<News> findAllByCategory(Category category) {
        LOGGER.info("SOAP method findAllByCategory() with PathParam = " + category.toString());
        return newsDao.findAllByCategory(category);
    }

    @Override
    @WebMethod
    public List<News> findAll() {
        LOGGER.info("SOAP method findAll()");
        return newsDao.findAll();
    }

    @Override
    @WebMethod
    public News findById(Integer id) {
        LOGGER.info("SOAP method findById() with parameter = " + id);
        return newsDao.findById(id).get();
    }

    @Override
    @WebMethod
    public Integer create(News model) {
        LOGGER.info("SOAP method create() for such model = " + model);
        return newsDao.create(model);
    }

    @Override
    @WebMethod
    public void update(News model) {
        LOGGER.info("SOAP method to update() for such model = " + model);
        newsDao.update(model);
    }
}
